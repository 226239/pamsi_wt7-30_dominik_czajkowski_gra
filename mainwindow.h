#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMessageBox>



namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    void Board_refresh();
    void Board_init();
    int first_empty();
    int rozmiar;
    void x(QPushButton *btt, int nr);

    ~MainWindow();

private slots:




    void on_pushButton_clicked();
    void on_pushButton_2_clicked();
    void on_pushButton_3_clicked();
    void on_pushButton_4_clicked();

    void on_pushButton_5_clicked();

    void on_pushButton_6_clicked();

    void on_pushButton_7_clicked();

    void on_pushButton_8_clicked();

    void on_pushButton_9_clicked();

    void on_rst_btt_clicked();



private:
    Ui::MainWindow *ui;
    QMessageBox *msg;


};



#endif // MAINWINDOW_H
