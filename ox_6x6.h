#ifndef OX_6X6_H
#define OX_6X6_H

#include <QWidget>
#include <vector>
#include <QPushButton>
#include <QDebug>
#include <QMessageBox>

namespace Ui {
class ox_6x6;
}

class ox_6x6 : public QWidget
{
    Q_OBJECT

public:
    explicit ox_6x6(QWidget *parent = 0);

    ~ox_6x6();

private:
    Ui::ox_6x6 *ui;
    QPushButton *arr[36];
    QMessageBox *msg;
    void Board_refresh();
    void Board_init();
    int first_empty();
    void y();
public slots:
    void btt1();
    void btt2();
    void btt3();
    void btt4();
    void btt5();
    void btt6();
    void btt7();
    void btt8();
    void btt9();
    void btt10();
    void btt11();
    void btt12();
    void btt13();
    void btt14();
    void btt15();
    void btt16();
    void btt17();
    void btt18();
    void btt19();
    void btt20();
    void btt21();
    void btt22();
    void btt23();
    void btt24();
    void btt25();
    void btt26();
    void btt27();
    void btt28();
    void btt29();
    void btt30();
    void btt31();
    void btt32();
    void btt33();
    void btt34();
    void btt35();
    void btt36();

private slots:
    void on_pushButton_clicked();
};

#endif // OX_6X6_H
