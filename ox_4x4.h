#ifndef OX_4X4_H
#define OX_4X4_H

#include <QWidget>
#include <vector>
#include <QPushButton>
#include <QDebug>
#include <QMessageBox>

namespace Ui {
class ox_4x4;
}

class ox_4x4 : public QWidget
{
    Q_OBJECT

public:
    explicit ox_4x4(QWidget *parent = 0);

    ~ox_4x4();

private:
    Ui::ox_4x4 *ui;
    QPushButton *arr[16];
    QMessageBox *msg;
    void Board_refresh();
    void Board_init();
    int first_empty();
    void x(QPushButton *btt, int nr);
    void y();
public slots:
    void btt1();
    void btt2();
    void btt3();
    void btt4();
    void btt5();
    void btt6();
    void btt7();
    void btt8();
    void btt9();
    void btt10();
    void btt11();
    void btt12();
    void btt13();
    void btt14();
    void btt15();
    void btt16();
private slots:
    void on_pushButton_clicked();
};

#endif // OX_4X4_H
