#-------------------------------------------------
#
# Project created by QtCreator 2017-06-07T14:12:10
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ox_graficzny
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    form1.cpp \
    ox_4x4.cpp \
    ox_5x5.cpp \
    ox_6x6.cpp

HEADERS  += mainwindow.h \
    form1.h \
    ox_4x4.h \
    ox_5x5.h \
    ox_6x6.h

FORMS    += mainwindow.ui \
    form1.ui \
    ox_4x4.ui \
    ox_5x5.ui \
    ox_6x6.ui
