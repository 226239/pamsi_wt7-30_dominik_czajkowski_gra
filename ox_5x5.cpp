#include "ox_5x5.h"
#include "ui_ox_5x5.h"

#include <queue>
#include <ctime>
#include <algorithm>

int input2;

char tab3[26];
bool czystart3 = false,czykoniec3 = false;

char player2 = 'O';
unsigned int licznik2=0;


std::queue<int> pozycjeO2;
std::queue<int> pozycjeX2;


/*
//wygrywanie dla 5 w lini
bool win(char t[], char z)
{
     bool a = false;

     //wiersze
     for (int i=1; i<=21; i+=5) a |= ((t[i]==z) && (t[i+1]==z) && (t[i+2]==z) && (t[i+3]==z) && (t[i+4]==z));

     //kolumny
     for (int i=1; i<=5; i++) a |= ((t[i]==z) && (t[i+5]==z) && (t[i+10]==z) && (t[i+15]==z) && (t[i+20]==z));

     //przekatna /
     a |= ((t[1]==z) && (t[7]==z) && (t[13]==z) && (t[19]==z) && (t[25]==z));

     //przekatna \
     a |= ((t[21]==z) && (t[17]==z) && (t[13]==z) && (t[9]==z) && (t[5]==z));

     if (a) return true;
     return false;
}
*/

//zwraca true po wygranej zawodnika
bool win2(char t[], char z)
{
    bool a=false;

    for (int i=1; i<=21; i+=5) a |= ((t[i]==z) && (t[i+1]==z) && (t[i+2]==z) && (t[i+3]==z) && (t[i+4]==z)); //wiersze od 1. kolumny

  //  for (int i=2; i<=17; i+=4) a|= ((t[i]==z) && (t[i+1]==z) && (t[i+2]==z) && (t[i+3]==z)); //wiersze od 2. kolumny

    for (int i=1; i<=5; i++) a |= ((t[i]==z) && (t[i+5]==z) && (t[i+10]==z) && (t[i+15]==z) && (t[i+20]==z)); //kolumny od 1. wiersza

  //  for (int i=5; i<=8; i++) a |= ((t[i]==z) && (t[i+4]==z) && (t[i+8]==z) && (t[i+12]==z)); //kolumny od 2. wiersza

    //przekatne
    int m[4] = {1, 2, 5, 7};      //malej¹ce przekatne
    int r[4] = {21, 17, 21, 22};  //rosnace przekatne

    for (int i=0; i<=0; i++)//1-7-13-19 && 2-8-14-20 && 6-12-18-24 && 7-13-19-15
    a |= ((t[m[i]]==z) && (t[m[i]+6]==z) && (t[m[i]+12]==z) && (t[m[i]+18]==z) && (t[m[i]+24]==z));

    for (int i=0; i<=0; i++)//16-12-8-4 && 17-13-9-5 && 21-17-13-9 && 22-18-14-10
    a |= ((t[r[i]]==z) && (t[r[i]-4]==z) && (t[r[i]-8]==z) && (t[r[i]-12]==z) && (t[r[i]-16]==z));

    if(a) {
        czykoniec3 = true;
        return true;
    }
    return false;
}


bool trzy_w_lini2(char t[], char z)
{
     bool a = false;
     //wiersze
    for (int i=1; i<=21; i+=5) a |= ((t[i]==z) && (t[i+1]==z) && (t[i+2]==z) ); //wiersze od 1. kolumny
    for (int i=2; i<=22; i+=5) a |= ((t[i]==z) && (t[i+1]==z) && (t[i+2]==z) ); //wiersze od 2. kolumny
    for (int i=3; i<=23; i+=5) a |= ((t[i]==z) && (t[i+1]==z) && (t[i+2]==z) ); //wiersze od 3. kolumny

    //kolumny
    for (int i=1; i<=5; i++) a |= ((t[i]==z) && (t[i+5]==z) && (t[i+10]==z) ); //kolumny od 1. wiersza
    for (int i=6; i<=10; i++) a |= ((t[i]==z) && (t[i+5]==z) && (t[i+10]==z) ); //kolumny od 2. wiersza
    for (int i=11; i<=15; i++) a |= ((t[i]==z) && (t[i+5]==z) && (t[i+10]==z) ); //kolumny od 3. wiersza

    //przekatne
    int m[9] = {1, 2, 3, 6, 7, 8, 11, 12, 13};      //malej¹ce przekatne
    int r[9] = {11, 12, 13, 16, 17, 18, 21, 22, 23};  //rosnace przekatne

    for (int i=0; i<=8; i++)//chyba ok
    a |= ( (t[m[i]]==z) && (t[m[i]+6]==z) && (t[m[i]+12]==z) );

    for (int i=0; i<=8; i++)//chyba ok
    a |= ( (t[r[i]]==z) && (t[r[i]-4]==z) && (t[r[i]-8]==z) );

    if(a) return true;
    return false;
}

//zwraca true, gdy nie ma juz wolnych miejsc na planszy
bool remis2(char t[])
{
     for (int i=1; i<=25; i++) if (t[i]==' ') return false;
     czykoniec3= true;
     return true;
}

//zwraca ilosc wolnych miejsc na planszy
int miejsca_wolne2(char t[])
{
    int mw=0;
    for (int i=1; i<=25; i++) if (t[i]==' ') mw++;
    return mw;
}


//pierwszy ruch komputera losuje
int ksiega_otwarc2(char t[])
{
    int pozycja;
    int zarodek = time(NULL);
    srand(zarodek);
    do {
        pozycja = rand() % 25 + 1;
        }while ( (t[pozycja]!=' ') );
    return pozycja;
}

//algorytm mini-max, tablica t[] to plansza z aktualnymi pozycjami gracza
//char player przekazuje znak gracza (X lub O)
int minimax2(char t[], char player, unsigned int poziom)
{
    int m=0, mmx=0;

    //sprawdzam ile jest wolnych miejsc na planszy
    int mw = miejsca_wolne2(t);

    //sprawdzam czy ktos wygral lub jes remis
    if (win2(t,player)) return (player == 'X') ? 1 : -1;//1 jesli komputer wygrywa, -1 jesli czlowiek
    if (remis2(t)) return 0;  //za remis 0

    //trzeba dopracowac funkcje heurystyczna, zmienne ograniczenie g³êbokoci rekursji
    if ((15<=mw) && (mw<=25)){
      if (poziom>7){
         if (((player=='O') && (m<mmx)) || ((player=='X') && (m>mmx))) mmx=m;
         return mmx;
       //if (trzy_w_lini(t,player)) return (player=='X') ? 1 : -1;
       }
    }
    else{
         if (poziom>5){
            if (((player=='O') && (m<mmx)) || ((player=='X') && (m>mmx))) mmx=m;
         return mmx;
         }
    }

    player = (player == 'X')? 'O':'X';   //zmiana graczy w celu przeanalizowania ruchow
    mmx = (player == 'X')? -25:25;//dla komputera liczymi max(mmx=-10), dla czlowieka min(mmx=10)
    for (int i =1; i<=25; i++){    //w kazdego pola na planszy
        if (t[i]==' '){           //ktore jest puste
           t[i] = player;         //wstawiam znak odpowiedniego gracza
           m = minimax2(t, player, poziom++);//i wyznaczam wartosc tego ruchu przez rekurencje
           t[i] = ' ';            //przywracam plansze do poprzedniego stanu
           if (((player=='O') && (m<mmx)) || ((player=='X') && (m>mmx))) mmx=m;
        }
    }
    return mmx;
}

//zwraca pozycje komputera obliczon¹ algorytmem minimaxowym
int komputer3(char t[])
{
    int pozycja, m, mmx;
    mmx = -25;                  //liczymy max dla komputera, wiec mmx = -10
    for (int i=1; i<=25; i++){   //w kazde pole planszy
        if (t[i] == ' '){       //ktore jest puste
           //cout << ".";
           t[i] = 'X';          //wpisuje znak komputera
           m = minimax2(t, 'X', 0); //i wyznaczam wartosc tej pozycji przez alg. mini-max
           t[i] = ' ';          //przywracam plansze do poprzedniego stanu
           //cout << "\nf.komputer : m["<<i<<"] = "<<m;
           //cout << "\nf.komputer : mmx["<<i<<"] = "<<mmx<<endl;
           if (m > mmx){
              mmx = m;
              pozycja = i;
           }
        }
    }
    //system("pause");
    return pozycja;
}

//void ruch(char t[], char &player, unsigned int licznik)
//{
//     int pozycja;
//     if (player == 'O'){ //jesli gra czlowiek
//   //     cout << "\nRuch gracza : ";
//        pozycja = input;
//        while ((pozycja < 1) && (pozycja > 25)){
//        //      cout << "Wybiez pole z zakresu 1 - 25! :";
//              pozycja = input;
//        }
////        while (t[pozycja] != ' '){
////              cout << "To pole jest juz zajete! Wybierz inne : ";
////              pozycja = kl;
//       // }
//        pozycjeO.push(pozycja);
//     }
//     else if (player == 'X'){  //jesli gra komputer

//             if (licznik > 1)
//                pozycja = komputer2(t);
//             else{
//                  pozycja = ksiega_otwarc(t);
//             }
//        //     cout << "\nRuch komputera : " << pozycja<<endl;
//             pozycjeX.push(pozycja);
//     }
//     if ((pozycja >= 1) && (pozycja <= 16) && (t[pozycja]==' ')) t[pozycja] = player;
//  //   else {cout<< "\ncos poszlo nie tak"<<endl; system("pause");}
//     player = (player == 'O') ? 'X' : 'O';   //zmiana gracza
//}

void ruch2(char t[], char &player, unsigned int licznik)
{
     int pozycja;
     if (player == 'O'){ //jesli gra czlowiek
        qDebug() << "\nRuch gracza : ";
        pozycja=input2;
        while ((pozycja < 1) && (pozycja > 25)){
              qDebug() << "Wybiez pole z zakresu 1 - 25! :";
              pozycja=input2;
        }
        while (t[pozycja] != ' '){
              qDebug() << "To pole jest juz zajete! Wybierz inne : ";
             pozycja = input2;
        }
        pozycjeO2.push(pozycja);
     }
     else if (player == 'X'){  //jesli gra komputer

             if (licznik > 1)
                pozycja = komputer3(t);
             else{
                  pozycja = ksiega_otwarc2(t);
             }
             qDebug() << "\nRuch komputera : " << pozycja<<endl;
             pozycjeX2.push(pozycja);
     }
     if ((pozycja >= 1) && (pozycja <= 25) && (t[pozycja]==' ')) t[pozycja] = player;
     else {qDebug()<< "\ncos poszlo nie tak"<<endl; system("pause");}
     player = (player == 'O') ? 'X' : 'O';   //zmiana gracza
}


ox_5x5::ox_5x5(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ox_5x5)
{
    msg = new QMessageBox;
    ui->setupUi(this);
    for(int j=0; j<5; j++)
    {
    arr[j] = new QPushButton;
    arr[j]->setGeometry(0,0,40,40);

    ui->horizontalLayout->addWidget(arr[j]);
    }

    for(int j=5; j<10; j++)
    {
    arr[j] = new QPushButton;
    arr[j]->setGeometry(0,0,100,100);


    ui->horizontalLayout_4->addWidget(arr[j]);

    }
    for(int j=10; j<15; j++)
    {
    arr[j] = new QPushButton;
    arr[j]->setGeometry(0,0,40,40);
    ui->horizontalLayout_5->addWidget(arr[j]);
    }
    for(int j=15; j<20; j++)
    {
    arr[j] = new QPushButton;
    arr[j]->setGeometry(0,0,40,40);
    ui->horizontalLayout_6->addWidget(arr[j]);
    }
    for(int j=20; j<25; j++)
    {
    arr[j] = new QPushButton;
    arr[j]->setGeometry(0,0,40,40);
    ui->horizontalLayout_7->addWidget(arr[j]);
    }

    //podpięcie sygnałów z klawiszy pola
    connect(arr[0],SIGNAL(clicked(bool)),this,SLOT(btt1()));
    connect(arr[1],SIGNAL(clicked(bool)),this,SLOT(btt2()));
    connect(arr[2],SIGNAL(clicked(bool)),this,SLOT(btt3()));
    connect(arr[3],SIGNAL(clicked(bool)),this,SLOT(btt4()));
    connect(arr[4],SIGNAL(clicked(bool)),this,SLOT(btt5()));
    connect(arr[5],SIGNAL(clicked(bool)),this,SLOT(btt6()));
    connect(arr[6],SIGNAL(clicked(bool)),this,SLOT(btt7()));
    connect(arr[7],SIGNAL(clicked(bool)),this,SLOT(btt8()));
    connect(arr[8],SIGNAL(clicked(bool)),this,SLOT(btt9()));
    connect(arr[9],SIGNAL(clicked(bool)),this,SLOT(btt10()));
    connect(arr[10],SIGNAL(clicked(bool)),this,SLOT(btt11()));
    connect(arr[11],SIGNAL(clicked(bool)),this,SLOT(btt12()));
    connect(arr[12],SIGNAL(clicked(bool)),this,SLOT(btt13()));
    connect(arr[13],SIGNAL(clicked(bool)),this,SLOT(btt14()));
    connect(arr[14],SIGNAL(clicked(bool)),this,SLOT(btt15()));
    connect(arr[15],SIGNAL(clicked(bool)),this,SLOT(btt16()));
    connect(arr[16],SIGNAL(clicked(bool)),this,SLOT(btt17()));
    connect(arr[17],SIGNAL(clicked(bool)),this,SLOT(btt18()));
    connect(arr[18],SIGNAL(clicked(bool)),this,SLOT(btt19()));
    connect(arr[19],SIGNAL(clicked(bool)),this,SLOT(btt20()));
    connect(arr[20],SIGNAL(clicked(bool)),this,SLOT(btt21()));
    connect(arr[21],SIGNAL(clicked(bool)),this,SLOT(btt22()));
    connect(arr[22],SIGNAL(clicked(bool)),this,SLOT(btt23()));
    connect(arr[23],SIGNAL(clicked(bool)),this,SLOT(btt24()));
    connect(arr[24],SIGNAL(clicked(bool)),this,SLOT(btt25()));

}

ox_5x5::~ox_5x5()
{
    delete ui;
    delete msg;
}

//*****************pole gry********************
void ox_5x5::btt1()
{
    input2=1;
    y();
}

void ox_5x5::btt2()
{
    input2=2;
    y();
}
void ox_5x5::btt3()
{
    input2=3;
    y();
}

void ox_5x5::btt4()
{
    input2=4;
    y();
}
void ox_5x5::btt5()
{
    input2=5;
    y();
}

void ox_5x5::btt6()
{
    input2=6;
    y();
}
void ox_5x5::btt7()
{
    input2=7;
    y();
}

void ox_5x5::btt8()
{
    input2=8;
    y();
}
void ox_5x5::btt9()
{
    input2=9;
    y();
}

void ox_5x5::btt10()
{
    input2=10;
    y();
}
void ox_5x5::btt11()
{
    input2=11;
    y();
}

void ox_5x5::btt12()
{
    input2=12;
    y();
}
void ox_5x5::btt13()
{
    input2=13;
    y();
}

void ox_5x5::btt14()
{
    input2=14;
    y();
}
void ox_5x5::btt15()
{
    input2=15;
    y();
}

void ox_5x5::btt16()
{
    input2=16;
    y();
}
void ox_5x5::btt17()
{
    input2=17;
    y();
}
void ox_5x5::btt18()
{
    input2=18;
    y();
}

void ox_5x5::btt19()
{
    input2=19;
    y();
}
void ox_5x5::btt20()
{
    input2=20;
    y();
}

void ox_5x5::btt21()
{
    input2=21;
    y();
}
void ox_5x5::btt22()
{
    input2=22;
    y();
}

void ox_5x5::btt23()
{
    input2=23;
    y();
}
void ox_5x5::btt24()
{
    input2=24;
    y();
}

void ox_5x5::btt25()
{
    input2=25;
    y();
}

void ox_5x5::Board_refresh()
{
    for(int i=1;i <=25;i++)
    {
    arr[i-1]->setText(" ");
    if (tab3[i] == 'X') arr[i-1]->setText("X");
    if (tab3[i] == 'O') arr[i-1]->setText("O");
    }
}

void ox_5x5::Board_init()
{
    for(int i=1; i<=25; i++)
        tab3[i] = ' ';
    Board_refresh();

}


int ox_5x5::first_empty()
{
    int tmp;
    for(int i=1; i<26; i++)
        if(tab3[i] == ' ') tmp = i;
    return tmp;
}



void ox_5x5::y()
{
    if(czystart3 == false)
    {
        Board_init();
        czystart3=true;
    }
    if (!win2(tab3,'X') && !win2(tab3,'O') && !remis2(tab3)){
        ruch2(tab3,player2,licznik2);
        if(win2(tab3,'O'))
        {
            msg->setText("Wygrywa kółko");
            msg->show();
        }
        if(win2(tab3,'X'))
        {
            msg->setText("Wygrywa krzyżyk");
            msg->show();
        }
        if(remis2(tab3))
        {
            msg->setText("Remis");
            msg->show();
        }
        Board_refresh();
        licznik2++;
    }
    else {
        if(win2(tab3,'O')) msg->setText("Wygrywa kółko");
        if(win2(tab3,'X')) msg->setText("Wygrywa krzyżyk");
        if(remis2(tab3)) msg->setText("Remis");
        msg->show();
    }
    if (!win2(tab3,'X') && !win2(tab3,'O') && !remis2(tab3)){
        ruch2(tab3,player2,licznik2);
        if(win2(tab3,'O'))
        {
            msg->setText("Wygrywa kółko");
            msg->show();
        }
        if(win2(tab3,'X'))
        {
            msg->setText("Wygrywa krzyżyk");
            msg->show();
        }
        if(remis2(tab3))
        {
            msg->setText("Remis");
            msg->show();
        }
        Board_refresh();
        licznik2++;
    }
    else {
        if(win2(tab3,'O')) msg->setText("Wygrywa kółko");
        if(win2(tab3,'X')) msg->setText("Wygrywa krzyżyk");
        if(remis2(tab3)) msg->setText("Remis");
        msg->show();
    }
}

void ox_5x5::on_pushButton_clicked()
{
    Board_init();
}
