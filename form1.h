#ifndef FORM1_H
#define FORM1_H

#include <QWidget>
#include "mainwindow.h"
#include "ox_4x4.h"
#include "ox_5x5.h"
#include "ox_6x6.h"

namespace Ui {
class Form1;
}

class Form1 : public QWidget
{
    Q_OBJECT

public:
    explicit Form1(QWidget *parent = 0);
    ~Form1();

private slots:
    void on_pushButton_clicked();

private:
    Ui::Form1 *ui;
    MainWindow *c;
    ox_4x4 *c2;
    ox_5x5 *c3;
    ox_6x6 *c4;
};

#endif // FORM1_H
