#include "ox_4x4.h"
#include "ui_ox_4x4.h"
#include <queue>
#include <ctime>
#include <algorithm>

int input;

char tab2[17];
bool czystart2 = false,czykoniec2 = false;
int kl;

char player = 'O';
unsigned int licznik=0;





//bool wygrana2(char t[], char g, bool cisza, int n)
//{


//    bool test;
//  int i;

//  test = false; // Zmienna przyjmuje true, jeśli zawodnik ma trzy figury
//                // w wierszu, kolumnie lub na przekątnych

//// Sprawdzamy wiersze

//  for(i = 1; i <= 13; i += 4) test |= ((t[i] == g) && (t[i+1] == g) && (t[i+2] == g) && (t[i+3] == g));

//// Sprawdzamy kolumny

//  for(i = 1; i <= 4; i++)    test |= ((t[i] == g) && (t[i+4] == g) && (t[i+8] == g) && (t[i+12] == g));

//// Sprawdzamy przekątną 1

//  test |= ((t[1] == g) && (t[6] == g) && (t[11] == g) && (t[16] == g));

//// Sprawdzamy przekątną 2

//  test |= ((t[4] == g) && (t[7] == g) && (t[10] == g) && (t[13] == g));

//  if(test)
//  {
//    if(!cisza)
//    {

//        czykoniec2 = true;

//    }

//    return true;

//  }
//  return false;
//}

//// Funkcja zwraca true, jeśli na planszy nie ma już
//// żadnego wolnego pola na ruch.
////-------------------------------------------------
//bool remis2(char t[], bool cisza, int n)
//{
//// Jeśli napotkamy spację, to plansza posiada wolne pola - zwracamy false

//  for(int i = 1; i <= 16; i++) if(t[i] == ' ') return false;

//// Jeśli pętla for zakończyła się normalnie, to na żadnym polu planszy nie było
//// spacji. Mamy do czynienia z remisem - zwracamy true

//  if(!cisza)
//  {
//      czykoniec2 = true;

//  }

//  return true;
//}

//// Algorytm rekurencyjny MINIMAX
//// Do algorytmu wchodzimy z planszą, na której ustawione jest pole
//// bieżącego gracza. Parametr gracz przekazuje literkę gracza, a
//// parametr mx przekazuje jego wynik w przypadku wygranej
////----------------------------------------------------------------
//int minimax2(char t[], char gracz, int n)
//{
//  int m, mmx;

//// Najpierw sprawdzamy, czy bieżący gracz wygrywa na planszy. Jeśli tak, to
//// zwracamy jego maksymalny wynik

//  if(wygrana2(t,gracz,true,n)) return (gracz == 'X') ? 1 : -1;

//// Następnie sprawdzamy, czy nie ma remisu. Jeśli jest, zwracamy wynik 0

//  if(remis2(t,true,n)) return 0;

//// Będziemy analizować możliwe posunięcia przeciwnika. Zmieniamy zatem
//// bieżącego gracza na jego przeciwnika

//  gracz = (gracz == 'X') ? 'O' : 'X';

//// Algorytm MINIMAX w kolejnych wywołaniach rekurencyjnych naprzemiennie analizuje
//// grę gracza oraz jego przeciwnika. Dla gracza oblicza maksimum wyniku gry, a dla
//// przeciwnika oblicza minimum. Wartość mmx ustawiamy w zależności od tego, czyje
//// ruchy analizujemy:
//// X - liczymy max, zatem mmx <- -10
//// O - liczymy min, zatem mmx <-  10

//  mmx = (gracz == 'O') ? 17 : -17;

//// Przeglądamy planszę szukając wolnych pół na ruch gracza. Na wolnym polu ustawiamy
//// literkę gracza i wyznaczamy wartość tego ruchu rekurencyjnym wywołaniem
//// algorytmu MINIMAX. Planszę przywracamy i w zależności kto gra:
//// X - wyznaczamy maximum
//// O - wyznaczamy minimum

//  for(int i = 1; i <= 16; i++)
//    if(t[i] == ' ')
//    {
//       t[i] = gracz;
//       m = minimax2(t,gracz,n);
//       t[i] = ' ';
//       if(((gracz == 'O') && (m < mmx)) || ((gracz == 'X') && (m > mmx))) mmx = m;
//    }
//  return mmx;
//}

//// Funkcja wyznacza ruch dla komputera.
////------------------------------------
//int komputer2(char t[], int n)
//{
//  int ruch, i, m, mmx;

//  mmx = -17;
//  for(i = 1; i <= 16; i++)
//    if(t[i] == ' ')
//    {
//      t[i] = 'X';
//      m = minimax2(t,'X',n);
//      t[i] = ' ';
//      if(m > mmx)
//      {
//        mmx = m; ruch = i;
//      }
//    }
//  return ruch;
//}


std::queue<int> pozycjeO;
std::queue<int> pozycjeX;


//pole gry u³o¿one w plansze 5 na 5 pol, wizualizacja
//void plansza(char t[])
//{
//     for (int i=1; i<=16; i++){
//         cout << " " << t[i] << " ";
//         if (i % 4) cout << "|";
//         else if (i != 16) cout << "\n---+---+---+---\n";
//         else cout << endl;
//     }
//}

/*
//wygrywanie dla 5 w lini
bool win(char t[], char z)
{
     bool a = false;

     //wiersze
     for (int i=1; i<=21; i+=5) a |= ((t[i]==z) && (t[i+1]==z) && (t[i+2]==z) && (t[i+3]==z) && (t[i+4]==z));

     //kolumny
     for (int i=1; i<=5; i++) a |= ((t[i]==z) && (t[i+5]==z) && (t[i+10]==z) && (t[i+15]==z) && (t[i+20]==z));

     //przekatna /
     a |= ((t[1]==z) && (t[7]==z) && (t[13]==z) && (t[19]==z) && (t[25]==z));

     //przekatna \
     a |= ((t[21]==z) && (t[17]==z) && (t[13]==z) && (t[9]==z) && (t[5]==z));

     if (a) return true;
     return false;
}
*/

//zwraca true po wygranej zawodnika
bool win(char t[], char z)
{
    bool a=false;

    for (int i=1; i<=16; i+=4) a |= ((t[i]==z) && (t[i+1]==z) && (t[i+2]==z) && (t[i+3]==z)); //wiersze od 1. kolumny

  //  for (int i=2; i<=17; i+=4) a|= ((t[i]==z) && (t[i+1]==z) && (t[i+2]==z) && (t[i+3]==z)); //wiersze od 2. kolumny

    for (int i=1; i<=4; i++) a |= ((t[i]==z) && (t[i+4]==z) && (t[i+8]==z) && (t[i+12]==z)); //kolumny od 1. wiersza

  //  for (int i=5; i<=8; i++) a |= ((t[i]==z) && (t[i+4]==z) && (t[i+8]==z) && (t[i+12]==z)); //kolumny od 2. wiersza

    //przekatne
    int m[4] = {1, 2, 5, 7};      //malej¹ce przekatne
    int r[4] = {13, 17, 21, 22};  //rosnace przekatne

    for (int i=0; i<=0; i++)//1-7-13-19 && 2-8-14-20 && 6-12-18-24 && 7-13-19-15
    a |= ((t[m[i]]==z) && (t[m[i]+5]==z) && (t[m[i]+10]==z) && (t[m[i]+15]==z));

    for (int i=0; i<=0; i++)//16-12-8-4 && 17-13-9-5 && 21-17-13-9 && 22-18-14-10
    a |= ((t[r[i]]==z) && (t[r[i]-3]==z) && (t[r[i]-6]==z) && (t[r[i]-9]==z));

    if(a) {
        czykoniec2 = true;
        return true;
    }
    return false;
}


bool trzy_w_lini(char t[], char z)
{
     bool a = false;
     //wiersze
    for (int i=1; i<=21; i+=5) a |= ((t[i]==z) && (t[i+1]==z) && (t[i+2]==z) ); //wiersze od 1. kolumny
    for (int i=2; i<=22; i+=5) a |= ((t[i]==z) && (t[i+1]==z) && (t[i+2]==z) ); //wiersze od 2. kolumny
    for (int i=3; i<=23; i+=5) a |= ((t[i]==z) && (t[i+1]==z) && (t[i+2]==z) ); //wiersze od 3. kolumny

    //kolumny
    for (int i=1; i<=5; i++) a |= ((t[i]==z) && (t[i+5]==z) && (t[i+10]==z) ); //kolumny od 1. wiersza
    for (int i=6; i<=10; i++) a |= ((t[i]==z) && (t[i+5]==z) && (t[i+10]==z) ); //kolumny od 2. wiersza
    for (int i=11; i<=15; i++) a |= ((t[i]==z) && (t[i+5]==z) && (t[i+10]==z) ); //kolumny od 3. wiersza

    //przekatne
    int m[9] = {1, 2, 3, 6, 7, 8, 11, 12, 13};      //malej¹ce przekatne
    int r[9] = {11, 12, 13, 16, 17, 18, 21, 22, 23};  //rosnace przekatne

    for (int i=0; i<=8; i++)//chyba ok
    a |= ( (t[m[i]]==z) && (t[m[i]+6]==z) && (t[m[i]+12]==z) );

    for (int i=0; i<=8; i++)//chyba ok
    a |= ( (t[r[i]]==z) && (t[r[i]-4]==z) && (t[r[i]-8]==z) );

    if(a) return true;
    return false;
}

//zwraca true, gdy nie ma juz wolnych miejsc na planszy
bool remis(char t[])
{
     for (int i=1; i<=16; i++) if (t[i]==' ') return false;
     czykoniec2 = true;
     return true;
}

//zwraca ilosc wolnych miejsc na planszy
int miejsca_wolne(char t[])
{
    int mw=0;
    for (int i=1; i<=16; i++) if (t[i]==' ') mw++;
    return mw;
}


//pierwszy ruch komputera losuje
int ksiega_otwarc(char t[])
{
    int pozycja;
    int zarodek = time(NULL);
    srand(zarodek);
    do {
        pozycja = rand() % 16 + 1;
        }while ( (t[pozycja]!=' ') );
    return pozycja;
}

//algorytm mini-max, tablica t[] to plansza z aktualnymi pozycjami gracza
//char player przekazuje znak gracza (X lub O)
int minimax(char t[], char player, unsigned int poziom)
{
    int m=0, mmx=0;

    //sprawdzam ile jest wolnych miejsc na planszy
    int mw = miejsca_wolne(t);

    //sprawdzam czy ktos wygral lub jes remis
    if (win(t,player)) return (player == 'X') ? 1 : -1;//1 jesli komputer wygrywa, -1 jesli czlowiek
    if (remis(t)) return 0;  //za remis 0

    //trzeba dopracowac funkcje heurystyczna, zmienne ograniczenie g³êbokoci rekursji
    if ((1<=mw) && (mw<=16)){
      if (poziom>7){
         if (((player=='O') && (m<mmx)) || ((player=='X') && (m>mmx))) mmx=m;
         return mmx;
       //if (trzy_w_lini(t,player)) return (player=='X') ? 1 : -1;
       }
    }
    else{
         if (poziom>5){
            if (((player=='O') && (m<mmx)) || ((player=='X') && (m>mmx))) mmx=m;
         return mmx;
         }
    }

    player = (player == 'X')? 'O':'X';   //zmiana graczy w celu przeanalizowania ruchow
    mmx = (player == 'X')? -16:16;//dla komputera liczymi max(mmx=-10), dla czlowieka min(mmx=10)
    for (int i =1; i<=16; i++){    //w kazdego pola na planszy
        if (t[i]==' '){           //ktore jest puste
           t[i] = player;         //wstawiam znak odpowiedniego gracza
           m = minimax(t, player, poziom++);//i wyznaczam wartosc tego ruchu przez rekurencje
           t[i] = ' ';            //przywracam plansze do poprzedniego stanu
           if (((player=='O') && (m<mmx)) || ((player=='X') && (m>mmx))) mmx=m;
        }
    }
    return mmx;
}

//zwraca pozycje komputera obliczon¹ algorytmem minimaxowym
int komputer2(char t[])
{
    int pozycja, m, mmx;
    mmx = -16;                  //liczymy max dla komputera, wiec mmx = -10
    for (int i=1; i<=16; i++){   //w kazde pole planszy
        if (t[i] == ' '){       //ktore jest puste
           //cout << ".";
           t[i] = 'X';          //wpisuje znak komputera
           m = minimax(t, 'X', 0); //i wyznaczam wartosc tej pozycji przez alg. mini-max
           t[i] = ' ';          //przywracam plansze do poprzedniego stanu
           //cout << "\nf.komputer : m["<<i<<"] = "<<m;
           //cout << "\nf.komputer : mmx["<<i<<"] = "<<mmx<<endl;
           if (m > mmx){
              mmx = m;
              pozycja = i;
           }
        }
    }
    //system("pause");
    return pozycja;
}

//void ruch(char t[], char &player, unsigned int licznik)
//{
//     int pozycja;
//     if (player == 'O'){ //jesli gra czlowiek
//   //     cout << "\nRuch gracza : ";
//        pozycja = input;
//        while ((pozycja < 1) && (pozycja > 25)){
//        //      cout << "Wybiez pole z zakresu 1 - 25! :";
//              pozycja = input;
//        }
////        while (t[pozycja] != ' '){
////              cout << "To pole jest juz zajete! Wybierz inne : ";
////              pozycja = kl;
//       // }
//        pozycjeO.push(pozycja);
//     }
//     else if (player == 'X'){  //jesli gra komputer

//             if (licznik > 1)
//                pozycja = komputer2(t);
//             else{
//                  pozycja = ksiega_otwarc(t);
//             }
//        //     cout << "\nRuch komputera : " << pozycja<<endl;
//             pozycjeX.push(pozycja);
//     }
//     if ((pozycja >= 1) && (pozycja <= 16) && (t[pozycja]==' ')) t[pozycja] = player;
//  //   else {cout<< "\ncos poszlo nie tak"<<endl; system("pause");}
//     player = (player == 'O') ? 'X' : 'O';   //zmiana gracza
//}

void ruch(char t[], char &player, unsigned int licznik)
{
     int pozycja;
     if (player == 'O'){ //jesli gra czlowiek
        qDebug() << "\nRuch gracza : ";
        pozycja=input;
        while ((pozycja < 1) && (pozycja > 25)){
              qDebug() << "Wybiez pole z zakresu 1 - 25! :";
              pozycja=input;
        }
        while (t[pozycja] != ' '){
              qDebug() << "To pole jest juz zajete! Wybierz inne : ";
             pozycja = input;
        }
        pozycjeO.push(pozycja);
     }
     else if (player == 'X'){  //jesli gra komputer

             if (licznik > 1)
                pozycja = komputer2(t);
             else{
                  pozycja = ksiega_otwarc(t);
             }
             qDebug() << "\nRuch komputera : " << pozycja<<endl;
             pozycjeX.push(pozycja);
     }
     if ((pozycja >= 1) && (pozycja <= 16) && (t[pozycja]==' ')) t[pozycja] = player;
     else {qDebug()<< "\ncos poszlo nie tak"<<endl; system("pause");}
     player = (player == 'O') ? 'X' : 'O';   //zmiana gracza
}


ox_4x4::ox_4x4(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ox_4x4)
{
    msg = new QMessageBox;
    ui->setupUi(this);
    for(int j=0; j<4; j++)
    {
    arr[j] = new QPushButton;
    arr[j]->setGeometry(0,0,40,40);

    ui->horizontalLayout->addWidget(arr[j]);
    }

    for(int j=4; j<8; j++)
    {
    arr[j] = new QPushButton;
    arr[j]->setGeometry(0,0,100,100);


    ui->horizontalLayout_2->addWidget(arr[j]);

    }
    for(int j=8; j<12; j++)
    {
    arr[j] = new QPushButton;
    arr[j]->setGeometry(0,0,40,40);
    ui->horizontalLayout_3->addWidget(arr[j]);
    }
    for(int j=12; j<16; j++)
    {
    arr[j] = new QPushButton;
    arr[j]->setGeometry(0,0,40,40);
    ui->horizontalLayout_4->addWidget(arr[j]);
    }

    //podpięcie sygnałów z klawiszy pola
    connect(arr[0],SIGNAL(clicked(bool)),this,SLOT(btt1()));
    connect(arr[1],SIGNAL(clicked(bool)),this,SLOT(btt2()));
    connect(arr[2],SIGNAL(clicked(bool)),this,SLOT(btt3()));
    connect(arr[3],SIGNAL(clicked(bool)),this,SLOT(btt4()));
    connect(arr[4],SIGNAL(clicked(bool)),this,SLOT(btt5()));
    connect(arr[5],SIGNAL(clicked(bool)),this,SLOT(btt6()));
    connect(arr[6],SIGNAL(clicked(bool)),this,SLOT(btt7()));
    connect(arr[7],SIGNAL(clicked(bool)),this,SLOT(btt8()));
    connect(arr[8],SIGNAL(clicked(bool)),this,SLOT(btt9()));
    connect(arr[9],SIGNAL(clicked(bool)),this,SLOT(btt10()));
    connect(arr[10],SIGNAL(clicked(bool)),this,SLOT(btt11()));
    connect(arr[11],SIGNAL(clicked(bool)),this,SLOT(btt12()));
    connect(arr[12],SIGNAL(clicked(bool)),this,SLOT(btt13()));
    connect(arr[13],SIGNAL(clicked(bool)),this,SLOT(btt14()));
    connect(arr[14],SIGNAL(clicked(bool)),this,SLOT(btt15()));
    connect(arr[15],SIGNAL(clicked(bool)),this,SLOT(btt16()));

}

ox_4x4::~ox_4x4()
{
    delete ui;
    delete msg;
}

//*****************pole gry********************
void ox_4x4::btt1()
{
    input=1;
    y();
}

void ox_4x4::btt2()
{
    input=2;
    y();
}
void ox_4x4::btt3()
{
    input=3;
    y();
}

void ox_4x4::btt4()
{
    input=4;
    y();
}
void ox_4x4::btt5()
{
    input=5;
    y();
}

void ox_4x4::btt6()
{
    input=6;
    y();
}
void ox_4x4::btt7()
{
    input=7;
    y();
}

void ox_4x4::btt8()
{
    input=8;
    y();
}
void ox_4x4::btt9()
{
    input=9;
    y();
}

void ox_4x4::btt10()
{
    input=10;
    y();
}
void ox_4x4::btt11()
{
    input=11;
    y();
}

void ox_4x4::btt12()
{
    input=12;
    y();
}
void ox_4x4::btt13()
{
    input=13;
    y();
}

void ox_4x4::btt14()
{
    input=14;
    y();
}
void ox_4x4::btt15()
{
    input=15;
    y();
}

void ox_4x4::btt16()
{
    input=16;
    y();
}

void ox_4x4::Board_refresh()
{
    for(int i=1;i <=16;i++)
    {
    arr[i-1]->setText(" ");
    if (tab2[i] == 'X') arr[i-1]->setText("X");
    if (tab2[i] == 'O') arr[i-1]->setText("O");
    }
}

void ox_4x4::Board_init()
{
    for(int i=1; i<=16; i++)
        tab2[i] = ' ';
    Board_refresh();

}

void ox_4x4::x(QPushButton *btt, int nr)
{

    if (!win(tab2,'X') && !win(tab2,'O') && !remis(tab2))
        if(tab2[nr] != 'X')
        {
            tab2[nr] = 'O';
            btt->setText("O");
            if(czykoniec2 == false)
            {
//                unsigned int i = komputer2(tab2);
//                qDebug() << "wyk";
//                if(tab2[i] != ' ') tab2[first_empty()] = 'X';
//                    else tab2[i] = 'X';
                ruch(tab2,player,licznik);
            }
            Board_refresh();
        }
    if(win(tab2,'X')) msg->setText("Wygrywa krzyżyk");
        else
             if(win(tab2,'O')) msg->setText("Wygrywa kółko");
                else
                    if(remis(tab2)) msg->setText("Remis");
    if (win(tab2,'X') && win(tab2,'O') && remis(tab2)) msg->show();
}

int ox_4x4::first_empty()
{
    int tmp;
    for(int i=1; i<17; i++)
        if(tab2[i] == ' ') tmp = i;
    return tmp;
}

void ox_4x4::on_pushButton_clicked()
{
    Board_init();
}

void ox_4x4::y()
{
    if(czystart2 == false)
    {
        Board_init();
        czystart2=true;
    }
    if (!win(tab2,'X') && !win(tab2,'O') && !remis(tab2)){
        ruch(tab2,player,licznik);
        if(win(tab2,'O'))
        {
            msg->setText("Wygrywa kółko");
            msg->show();
        }
        if(win(tab2,'X'))
        {
            msg->setText("Wygrywa krzyżyk");
            msg->show();
        }
        if(remis(tab2))
        {
            msg->setText("Remis");
            msg->show();
        }
        Board_refresh();
        licznik++;
    }
    else {
        if(win(tab2,'O')) msg->setText("Wygrywa kółko");
        if(win(tab2,'X')) msg->setText("Wygrywa krzyżyk");
        if(remis(tab2)) msg->setText("Remis");
        msg->show();
    }
    if (!win(tab2,'X') && !win(tab2,'O') && !remis(tab2)){
        ruch(tab2,player,licznik);
        if(win(tab2,'O'))
        {
            msg->setText("Wygrywa kółko");
            msg->show();
        }
        if(win(tab2,'X'))
        {
            msg->setText("Wygrywa krzyżyk");
            msg->show();
        }
        if(remis(tab2))
        {
            msg->setText("Remis");
            msg->show();
        }
        Board_refresh();
        licznik++;
    }
    else {
        if(win(tab2,'O')) msg->setText("Wygrywa kółko");
        if(win(tab2,'X')) msg->setText("Wygrywa krzyżyk");
        if(remis(tab2)) msg->setText("Remis");
        msg->show();
    }
}
