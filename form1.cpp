#include "form1.h"
#include "ui_form1.h"
#include "mainwindow.h"
#include "ox_4x4.h"
#include <QDebug>

Form1::Form1(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Form1)
{
    ui->setupUi(this);
    c = new MainWindow;
    c2 = new ox_4x4;
    c3 = new ox_5x5;
    c4 = new ox_6x6;
}

Form1::~Form1()
{
    delete ui;
    delete c;
    delete c2;
    delete c3;
    delete c4;
}

void Form1::on_pushButton_clicked()
{


    switch(ui->rozm_in->currentIndex())
    {
    case 0:
        c->show();
        break;
    case 1:
        c2->show();
        break;
    case 2:
        c3->show();
        break;
    case 3:
        c4->show();
        break;
    }




}
