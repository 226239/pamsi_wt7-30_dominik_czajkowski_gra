#ifndef OX_5X5_H
#define OX_5X5_H

#include <QWidget>
#include <vector>
#include <QPushButton>
#include <QDebug>
#include <QMessageBox>

namespace Ui {
class ox_5x5;
}

class ox_5x5 : public QWidget
{
    Q_OBJECT

public:
    explicit ox_5x5(QWidget *parent = 0);

    ~ox_5x5();

private:
    Ui::ox_5x5 *ui;
    QPushButton *arr[25];
    QMessageBox *msg;
    void Board_refresh();
    void Board_init();
    int first_empty();
    void x(QPushButton *btt, int nr);
    void y();
public slots:
    void btt1();
    void btt2();
    void btt3();
    void btt4();
    void btt5();
    void btt6();
    void btt7();
    void btt8();
    void btt9();
    void btt10();
    void btt11();
    void btt12();
    void btt13();
    void btt14();
    void btt15();
    void btt16();
    void btt17();
    void btt18();
    void btt19();
    void btt20();
    void btt21();
    void btt22();
    void btt23();
    void btt24();
    void btt25();
private slots:

    void on_pushButton_clicked();
};
#endif // OX_5X5_H
