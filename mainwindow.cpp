#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "window4x4.h"
#include "form1.h"
#include <QDebug>
#include <QWidget>


char tab[11];
bool czystart,czykoniec;




bool wygrana(char t[], char g, bool cisza, int n)
{
int s =n;
s++;

    bool test;
  int i;

  test = false; // Zmienna przyjmuje true, jeśli zawodnik ma trzy figury
                // w wierszu, kolumnie lub na przekątnych

// Sprawdzamy wiersze

  for(i = 1; i <= 7; i += 3) test |= ((t[i] == g) && (t[i+1] == g) && (t[i+2] == g));

// Sprawdzamy kolumny

  for(i = 1; i <= 3; i++)    test |= ((t[i] == g) && (t[i+3] == g) && (t[i+6] == g));

// Sprawdzamy przekątną 1-5-9

  test |= ((t[1] == g) && (t[5] == g) && (t[9] == g));

// Sprawdzamy przekątną 3-5-7

  test |= ((t[3] == g) && (t[5] == g) && (t[7] == g));

  if(test)
  {
    if(!cisza)
    {

        czykoniec = true;

    }

    return true;

  }
  return false;
}

// Funkcja zwraca true, jeśli na planszy nie ma już
// żadnego wolnego pola na ruch.
//-------------------------------------------------
bool remis(char t[], bool cisza, int n)
{
// Jeśli napotkamy spację, to plansza posiada wolne pola - zwracamy false

  for(int i = 1; i <= (n*n); i++) if(t[i] == ' ') return false;

// Jeśli pętla for zakończyła się normalnie, to na żadnym polu planszy nie było
// spacji. Mamy do czynienia z remisem - zwracamy true

  if(!cisza)
  {
      czykoniec = true;

  }

  return true;
}

// Algorytm rekurencyjny MINIMAX
// Do algorytmu wchodzimy z planszą, na której ustawione jest pole
// bieżącego gracza. Parametr gracz przekazuje literkę gracza, a
// parametr mx przekazuje jego wynik w przypadku wygranej
//----------------------------------------------------------------
int minimax(char t[], char gracz, int n)
{
  int m, mmx;

// Najpierw sprawdzamy, czy bieżący gracz wygrywa na planszy. Jeśli tak, to
// zwracamy jego maksymalny wynik

  if(wygrana(t,gracz,true,n)) return (gracz == 'X') ? 1 : -1;

// Następnie sprawdzamy, czy nie ma remisu. Jeśli jest, zwracamy wynik 0

  if(remis(t,true,n)) return 0;

// Będziemy analizować możliwe posunięcia przeciwnika. Zmieniamy zatem
// bieżącego gracza na jego przeciwnika

  gracz = (gracz == 'X') ? 'O' : 'X';

// Algorytm MINIMAX w kolejnych wywołaniach rekurencyjnych naprzemiennie analizuje
// grę gracza oraz jego przeciwnika. Dla gracza oblicza maksimum wyniku gry, a dla
// przeciwnika oblicza minimum. Wartość mmx ustawiamy w zależności od tego, czyje
// ruchy analizujemy:
// X - liczymy max, zatem mmx <- -10
// O - liczymy min, zatem mmx <-  10

  mmx = (gracz == 'O') ? 10 : -10;

// Przeglądamy planszę szukając wolnych pół na ruch gracza. Na wolnym polu ustawiamy
// literkę gracza i wyznaczamy wartość tego ruchu rekurencyjnym wywołaniem
// algorytmu MINIMAX. Planszę przywracamy i w zależności kto gra:
// X - wyznaczamy maximum
// O - wyznaczamy minimum

  for(int i = 1; i <= 9; i++)
    if(t[i] == ' ')
    {
       t[i] = gracz;
       m = minimax(t,gracz,n);
       t[i] = ' ';
       if(((gracz == 'O') && (m < mmx)) || ((gracz == 'X') && (m > mmx))) mmx = m;
    }
  return mmx;
}

// Funkcja wyznacza ruch dla komputera.
//------------------------------------
int komputer(char t[], int n)
{
  int ruch, i, m, mmx;

  mmx = -10;
  for(i = 1; i <= 9; i++)
    if(t[i] == ' ')
    {
      t[i] = 'X';
      m = minimax(t,'X',n);
      t[i] = ' ';
      if(m > mmx)
      {
        mmx = m; ruch = i;
      }
    }
  return ruch;
}

//bool wygrana(char t[], char g, bool cisza, int n)
//{

//bool war=false;


//  bool test;
//  int i;

//  test = false; // Zmienna przyjmuje true, jeśli zawodnik ma trzy figury
//                // w wierszu, kolumnie lub na przekątnych

//// Sprawdzamy wiersze

//  for(i = 1; i <= (n*n-n); i += 3)
//      test |= ((t[i] == g) && (t[i+1] == g) && (t[i+2] == g));

//// Sprawdzamy kolumny

//  for(i = 1; i <= n; i++)
//      test |= ((t[i] == g) && (t[i+3] == g) && (t[i+6] == g));

////// Sprawdzamy przekątną 1-5-9

////  for(int i=1; i<=n*n; i+=(n+1))
////  {
////      if(t[i] == g) war=true;
////        else break;
////  }

////  test |= war;

////// Sprawdzamy przekątną 3-5-7

////  for(int i=3; i<=n*n; i+=(n-1))
////  {
////      if(t[i] == g)
////      {
////               qDebug() << "debug:" << war << endl;
////      }
////        else break;

// // }

//  // Sprawdzamy przekątną 1-5-9

//    test |= ((t[1] == g) && (t[5] == g) && (t[9] == g));

//  // Sprawdzamy przekątną 3-5-7

//    test |= ((t[3] == g) && (t[5] == g) && (t[7] == g));

////  test |= war;

//  if(test)
//  {
//    if(!cisza)
//    {

//        czykoniec = true;

//    }

//    return true;

//  }
//  return false;
//}

//// Funkcja zwraca true, jeśli na planszy nie ma już
//// żadnego wolnego pola na ruch.
////-------------------------------------------------
//bool remis(char t[], bool cisza, int n)
//{
//// Jeśli napotkamy spację, to plansza posiada wolne pola - zwracamy false

//  for(int i = 1; i <= n*n; i++) if(t[i] == ' ') return false;

//// Jeśli pętla for zakończyła się normalnie, to na żadnym polu planszy nie było
//// spacji. Mamy do czynienia z remisem - zwracamy true

//  if(!cisza)
//  {
//      czykoniec = true;

//  }

//  return true;
//}

//// Algorytm rekurencyjny MINIMAX
//// Do algorytmu wchodzimy z planszą, na której ustawione jest pole
//// bieżącego gracza. Parametr gracz przekazuje literkę gracza, a
//// parametr mx przekazuje jego wynik w przypadku wygranej
////----------------------------------------------------------------
//int minimax(char t[], char gracz, int n)
//{
//  int m, mmx;

//// Najpierw sprawdzamy, czy bieżący gracz wygrywa na planszy. Jeśli tak, to
//// zwracamy jego maksymalny wynik

//  if(wygrana(t,gracz,true, n)) return (gracz == 'X') ? 1 : -1;

//// Następnie sprawdzamy, czy nie ma remisu. Jeśli jest, zwracamy wynik 0

//  if(remis(t,true, n)) return 0;

//// Będziemy analizować możliwe posunięcia przeciwnika. Zmieniamy zatem
//// bieżącego gracza na jego przeciwnika

//  gracz = (gracz == 'X') ? 'O' : 'X';

//// Algorytm MINIMAX w kolejnych wywołaniach rekurencyjnych naprzemiennie analizuje
//// grę gracza oraz jego przeciwnika. Dla gracza oblicza maksimum wyniku gry, a dla
//// przeciwnika oblicza minimum. Wartość mmx ustawiamy w zależności od tego, czyje
//// ruchy analizujemy:
//// X - liczymy max, zatem mmx <- -10
//// O - liczymy min, zatem mmx <-  10

//  mmx = (gracz == 'O') ? n*n+1 : -(n*n+1);

//// Przeglądamy planszę szukając wolnych pół na ruch gracza. Na wolnym polu ustawiamy
//// literkę gracza i wyznaczamy wartość tego ruchu rekurencyjnym wywołaniem
//// algorytmu MINIMAX. Planszę przywracamy i w zależności kto gra:
//// X - wyznaczamy maximum
//// O - wyznaczamy minimum

//  for(int i = 1; i <= n; i++)
//    if(t[i] == ' ')
//    {
//       t[i] = gracz;
//       m = minimax(t,gracz,n);
//       t[i] = ' ';
//       if(((gracz == 'O') && (m < mmx)) || ((gracz == 'X') && (m > mmx))) mmx = m;
//    }
//  return mmx;
//}

//// Funkcja wyznacza ruch dla komputera.
////------------------------------------
//int komputer(char t[], int n)
//{
//  int ruch, i, m, mmx;

//  mmx = -10;
//  for(i = 1; i <= 9; i++)
//    if(t[i] == ' ')
//    {
//      t[i] = 'X';
//      m = minimax(t,'X',n);
//      t[i] = ' ';
//      if(m > mmx)
//      {
//        mmx = m; ruch = i;
//      }
//    }
//  return ruch;
//}

void MainWindow::Board_refresh()
{

    ui->pushButton->setText(" ");
    ui->pushButton_2->setText(" ");
    ui->pushButton_3->setText(" ");
    ui->pushButton_4->setText(" ");
    ui->pushButton_5->setText(" ");
    ui->pushButton_6->setText(" ");
    ui->pushButton_7->setText(" ");
    ui->pushButton_8->setText(" ");
    ui->pushButton_9->setText(" ");
    if (tab[1] == 'X') ui->pushButton->setText("X");
    if (tab[1] == 'O') ui->pushButton->setText("O");
    if (tab[2] == 'X') ui->pushButton_2->setText("X");
    if (tab[2] == 'O') ui->pushButton_2->setText("O");
    if (tab[3] == 'X') ui->pushButton_3->setText("X");
    if (tab[3] == 'O') ui->pushButton_3->setText("O");
    if (tab[4] == 'X') ui->pushButton_4->setText("X");
    if (tab[4] == 'O') ui->pushButton_4->setText("O");
    if (tab[5] == 'X') ui->pushButton_5->setText("X");
    if (tab[5] == 'O') ui->pushButton_5->setText("O");
    if (tab[6] == 'X') ui->pushButton_6->setText("X");
    if (tab[6] == 'O') ui->pushButton_6->setText("O");
    if (tab[7] == 'X') ui->pushButton_7->setText("X");
    if (tab[7] == 'O') ui->pushButton_7->setText("O");
    if (tab[8] == 'X') ui->pushButton_8->setText("X");
    if (tab[8] == 'O') ui->pushButton_8->setText("O");
    if (tab[9] == 'X') ui->pushButton_9->setText("X");
    if (tab[9] == 'O') ui->pushButton_9->setText("O");

}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    msg = new QMessageBox;
    rozmiar=3;
}

MainWindow::~MainWindow()
{
    delete ui;
    delete msg;

}

void MainWindow::Board_init()
{
    for(int i=1; i<=10; i++)
        tab[i] = ' ';
    Board_refresh();
}


//**************przyciski pola gry***********************
void MainWindow::on_pushButton_clicked()
{
    if(czystart == false)
    {
        czystart = true;
        Board_init();
    }
    x(ui->pushButton,1);
}

void MainWindow::on_pushButton_2_clicked()
{
    if(czystart == false)
    {
        czystart = true;
        Board_init();
    }
    x(ui->pushButton_2,2);
}

void MainWindow::on_pushButton_3_clicked()
{
    if(czystart == false)
    {
        czystart = true;
        Board_init();
    }
    x(ui->pushButton_3,3);
}

void MainWindow::on_pushButton_4_clicked()
{
    if(czystart == false)
    {
        czystart = true;
        Board_init();
    }
    x(ui->pushButton_4,4);
}

void MainWindow::on_pushButton_5_clicked()
{
    if(czystart == false)
    {
        czystart = true;
        Board_init();
    }
    x(ui->pushButton_5,5);
}

void MainWindow::on_pushButton_6_clicked()
{
    if(czystart == false)
    {
        czystart = true;
        Board_init();
    }
    x(ui->pushButton_6,6);
}

void MainWindow::on_pushButton_7_clicked()
{
    if(czystart == false)
    {
        czystart = true;
        Board_init();
    }
    x(ui->pushButton_7,7);
}

void MainWindow::on_pushButton_8_clicked()
{
    if(czystart == false)
    {
        czystart = true;
        Board_init();
    }
    x(ui->pushButton_8,8);
}

void MainWindow::on_pushButton_9_clicked()
{
    if(czystart == false)
    {
        czystart = true;
        Board_init();
    }
    x(ui->pushButton_9,9);
}

void MainWindow::on_rst_btt_clicked()
{
    czykoniec = false;
    czystart = true;
    Board_init();
    Board_refresh();

}

int MainWindow::first_empty()
{
    int tmp;
    for(int i=1; i<10; i++)
        if(tab[i] == ' ') tmp = i;
    return tmp;
}

void MainWindow::x(QPushButton *btt, int nr)
{
    if(czystart == false)
    {
        czystart = true;
        Board_init();
    }
    if (!wygrana(tab,'X',false,rozmiar) && !wygrana(tab,'O',false,rozmiar) && !remis(tab,false,rozmiar))
        if(tab[nr] != 'X')
        {
            tab[nr] = 'O';
            btt->setText("O");
            if(czykoniec == false)
            {
                unsigned int i = komputer(tab,rozmiar);
                if(tab[i%(rozmiar*rozmiar)] != ' ') tab[first_empty()] = 'X';
                    else tab[i%(rozmiar*rozmiar)] = 'X';
            }
            Board_refresh();
        }
    if(wygrana(tab,'X',false,rozmiar)) msg->setText("Wygrywa krzyżyk");
        else
             if(wygrana(tab,'O',false,rozmiar)) msg->setText("Wygrywa kółko");
                else
                    if(remis(tab,false,rozmiar)) msg->setText("Remis");
    if(czykoniec == true) msg->show();
}

